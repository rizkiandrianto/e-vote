<?php
if(!isset($_SESSION['id_admin'])) {
   header('location: ../');
}

$id = strip_tags(mysqli_real_escape_string($con, $_GET['id']));
$sql = $con->prepare("SELECT * FROM t_kandidat WHERE id_kandidat = ?") or die($con->error);
$sql->bind_param('i', $id);
$sql->execute();
$sql->store_result();
$sql->bind_result($id, $nama, $foto, $visi, $misi, $suara, $periode);
$sql->fetch();
?>
<h3>Detail Kandidat</h3>
<hr />
<div class="row">
   <div class="medium-3 columns">
      <div class="callout text-center">
         <img src="../assets/img/kandidat/<?php echo $foto; ?>" alt="">
      </div>
   </div>
   <div class="medium-9 columns" style="padding-top:20px;">
      <table>
         <tbody>
            <tr>
               <td>Nama Kandidat</td>
               <td>: <?php echo $nama; ?></td>
            </tr>
            <tr>
               <td>Visi</td>
               <td>: <?php echo $visi; ?></td>
            </tr>
            <tr>
               <td>Misi</td>
               <td>: <?php echo $misi; ?></td>
            </tr>
            <tr>
               <td>Jumlah Suara</td>
               <td>: <?php echo $suara; ?></td>
            </tr>
            <tr>
               <td>Periode</td>
               <td>: <?php echo $periode; ?></td>
            </tr>
         </tbody>
      </table>
   </div>
   <div class="medium-9 medium-offset-2 columns">
      <a href="?page=kandidat" class="button alert">Kembali</a>
      <a href="?page=kandidat&action=edit&id=<?php echo $id; ?>" class="button">Edit</a>
   </div>
</div>
